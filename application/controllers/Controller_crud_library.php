<?php
class Controller_crud_library extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Model_crud_library');
    }

    function c_select() {
        $view_data['crud_list'] = $this->Model_crud_library->m_select();
        $this->load->view('View_header');
        $this->load->view('View_select', $view_data);
        $this->load->view('View_pages');
        $this->load->view('View_footer');
    }

    function c_select_genre() {
        $view_data['crud_list'] = $this->Model_crud_library->m_select_genre();
        $this->load->view('View_header');
        $this->load->view('View_select_genre', $view_data);
        $this->load->view('View_pages');
        $this->load->view('View_footer');
    }

    function c_select_two() {
        $view_data['crud_list'] = $this->Model_crud_library->m_select_two();
        $this->load->view('View_header');
        $this->load->view('View_select_2', $view_data);
        $this->load->view('View_pages');
        $this->load->view('View_footer');
    }


    function c_insert() {
        $view_data['genre_list'] = $this->Model_crud_library->m_select_genre();
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('index', 'indeks', 'required');
        $this->form_validation->set_rules('title', 'Tytuł', 'required');
        $this->form_validation->set_rules('author', 'Autor', 'required');
        $this->form_validation->set_rules('genre_id', 'ID gatunku', 'required');
        $this->form_validation->set_rules('pages', 'Liczba stron', 'required');
        $this->form_validation->set_rules('year', 'Rok wydania', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('View_header');
            $this->load->view('View_insert', $view_data);
            $this->load->view('View_pages');
            $this->load->view('View_footer');
        }
        else {
            $this->Model_crud_library->m_insert();
            $this->c_select();
        }
    }

    function c_insert_genre() {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('genre_name', 'Gatunek', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('View_header');
            $this->load->view('View_insert_genre');
            $this->load->view('View_pages');
            $this->load->view('View_footer');
        }
        else {
            $this->Model_crud_library->m_insert_genre();
            $this->c_select_genre();
        }
    }

    function c_update_form($id) {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Tytuł', 'required');
        $view_data['crud_list'] = $this->Model_crud_library->m_select_by_id($id);
        $this->load->view('View_header');
        $this->load->view('View_update', $view_data);
        $this->load->view('View_pages');
        $this->load->view('View_footer');
    }

    function c_update_genre_form($id){
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('genre_name', 'Gatunek', 'required');
        $view_data['crud_list'] = $this->Model_crud_library->m_select_genre_by_id($id);
        $this->load->view('View_header');
        $this->load->view('View_update_genre', $view_data);
        $this->load->view('View_pages');
        $this->load->view('View_footer');
    }

    function c_update() {
        $view_data['genre_list'] = $this->Model_crud_library->m_select_genre();
        $this->Model_crud_library->m_update();
        $this->c_select();
    }

    function c_update_genre() {
        $this->Model_crud_library->m_update_genre();
        $this->c_select_genre();
    }

    function c_delete($id) {
        $this->Model_crud_library->m_delete($id);
        if ($this->db->affected_rows() == 1) {
            $this->load->view('View_header');
            $this->load->view('View_success');
            $this->load->helper('url');
            $url = $this->config->base_url();
            $controller = $this->router->fetch_class();
            $data = array(
                'url' => $url,
                'controller' => $controller
            );
            $this->load->view('View_link', $data);
            $this->load->view('View_footer');
        } else {
            $this->load->view('View_header');
            $this->load->view('View_failure');
            $this->load->view('View_footer');
        }
    }

    function c_delete_genre($id) {
        $this->Model_crud_library->m_delete_genre($id);
        if ($this->db->affected_rows() == 1) {
            $this->load->view('View_header');
            $this->load->view('View_success');
            $this->load->helper('url');
            $url = $this->config->base_url();
            $controller = $this->router->fetch_class();
            $data = array(
                'url' => $url,
                'controller' => $controller
            );
            $this->load->view('View_link_genre', $data);
            $this->load->view('View_footer');
        } else {
            $this->load->view('View_header');
            $this->load->view('View_failure');
            $this->load->view('View_footer');
        }
    }
}
