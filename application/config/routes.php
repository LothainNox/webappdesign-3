<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Welcome';

$route['books/select'] = 'Controller_crud_library/c_select';
$route['books/select_two'] = 'Controller_crud_library/c_select_two';
$route['books/select_genre'] = 'Controller_crud_library/c_select_genre';

$route['books/add'] = 'Controller_crud_library/c_insert';
$route['books/add_genre'] = 'Controller_crud_library/c_insert_genre';

$route['books/update'] = 'Controller_crud_library/c_update';
$route['books/update_genre'] = 'Controller_crud_library/c_update_genre';

$route['books/update_form'] = 'Controller_crud_library/c_update_form';
$route['books/update_genre_form'] = 'Controller_crud_library/c_update_genre_form';

$route['books/delete/(:num)'] = 'Controller_crud_library/c_delete/$1';
$route['books/delete_genre/(:num)'] = 'Controller_crud_library/c_delete_genre/$1';

$route['books/select/(:num)'] = 'Controller_crud_library/c_select/$1';
$route['books/select_genre/(:num)'] = 'Controller_crud_library/c_select_genre/$1';

$route['books/update_form/(:num)'] = 'Controller_crud_library/c_update_form/$1';
$route['books/update_genre_form/(:num)'] = 'Controller_crud_library/c_update_genre_form/$1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

