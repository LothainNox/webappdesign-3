<?php
class Model_crud_library extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function m_select() {

        $this->db->select('*');
        $this->db->from('crudBiblioteka');
        $this->db->order_by('index');
        $query = $this->db->get();

        if ($query->num_rows() > 0)
            $crud = $query->result_array();

        return $crud;
    }

    function m_select_genre() {

        $this->db->select('*');
        $this->db->from('bookGenres');
        $this->db->order_by('genre_index');
        $query = $this->db->get();

        if ($query->num_rows() > 0)
            $crud = $query->result_array();

        return $crud;
    }

    function m_select_by_id($id) {
        $this->db->where('index', $id);
        $query = $this->db->get('crudBiblioteka');
        $row = $query->row();
        return $row;
    }

    function m_select_genre_by_id($id) {
        $this->db->where('genre_index', $id);
        $query = $this->db->get('bookGenres');
        $row = $query->row();
        return $row;
    }

    function m_select_two() {
        $this->db->select('*');
        $this->db->from('crudBiblioteka');
        $this->db->join('bookGenres', 'crudBiblioteka,genre_id = bookGenres.genre_index','JOIN Type');
        $query = $this->db->get();

        if ($query->num_rows() > 0)
            $crud = $query->result_array();

        return $crud;

    }


    function m_insert() {
        $this->load->helper('url');
        $data = array(
            'index' => $this->input->post('index'),
            'title' => $this->input->post('title'),
            'author' => $this->input->post('author'),
            'genre_id' => $this->input->post('genre_id'),
            'pages' => $this->input->post('pages'),
            'year' => $this->input->post('year'),
        );
        return $this->db->insert('crudBiblioteka', $data);
    }

    function m_insert_genre() {
        $this->load->helper('url');
        $data = array(
            'genre_index' => $this->input->post('genre_index'),
            'genre_name' => $this->input->post('genre_name'),
        );
        return $this->db->insert('bookGenres', $data);
    }

    function m_update() {
        $data = array(
            'index' => $this->input->post('index'),
            'title' => $this->input->post('title'),
            'author' => $this->input->post('author'),
            'genre_id' => $this->input->post('genre_id'),
            'pages' => $this->input->post('pages'),
            'year' => $this->input->post('year')
        );
        $this->db->where('index', $data['index']);
        return $this->db->update('crudBiblioteka', $data);
    }

    function m_update_genre() {
        $data = array(
            'genre_index' => $this->input->post('genre_index'),
            'genre_name' => $this->input->post('genre_name'),
        );
        $this->db->where('genre_index', $data['genre_index']);
        return $this->db->update('bookGenres', $data);
    }

    function m_delete($id) {
        $this->db->where('index', $id);
        return $this->db->delete('crudBiblioteka');
    }

    function m_delete_genre($id) {
        $this->db->where('genre_index', $id);
        return $this->db->delete('bookGenres');
    }
}
