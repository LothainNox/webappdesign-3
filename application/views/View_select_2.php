<h2 class="text-center">Lista wszystkich książek wraz z gatunkami</h2>
<table class="table table-dark table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th>Lp</th>
            <th>Indeks</th>
            <th>Tytuł</th>
            <th>Autor</th>
            <th>Gatunek</th>
            <th>Liczba stron</th>
            <th>Rok wydania</th>
            <th colspan="2"></th>
        </tr>
    </thead>
    <tbody>
    <?php $i = 1; ?>
    <?php foreach ($crud_list as $el) { ?>
        <tr>
            <td> <?php echo ($i); $i = $i + 1 ?> </td>
            <td> <?php echo $el['index'] ?> </td>
            <td> <?php echo $el['title'] ?> </td>
            <td> <?php echo $el['author'] ?> </td>
            <td> <?php echo $el['genre_name'] ?> </td>
            <td> <?php echo $el['pages'] ?> </td>
            <td> <?php echo $el['year'] ?> </td>
            <td><a href="update_form/<?php echo $el['index'] ?>" class="btn btn-warning">Edytuj</a></td>
            <td><a href="delete/<?php echo $el['index'] ?>" class="btn btn-danger">Kasuj</a></td>
        </tr>
    <?php } ?>
    <tfoot>
        <tr>
            <th colspan="100%"><a href="add" class="btn btn-success"> Dodaj </a></th>
        </tr>
    </tfoot>
    </tbody>
</table>
