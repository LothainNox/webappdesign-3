<h2 class="text-center"> Edycja książki o ID = <?php echo $crud_list->index ?> </h2>
<?php echo validation_errors(); ?>
<?php echo form_open('books/update') ?>

    <table class="table table-dark table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th>Pole</th>
            <th>Zawartość</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><label for="index">indeks</label></td>
            <td><input class="bg-dark text-white" type="text" name="index" readonly value="<?php echo $crud_list->index ?>" /> </td>
        </tr>
        <tr>
            <td><label for="title">Tytuł</label></td>
            <td><input class="bg-dark text-white" type="text" name="title" value="<?php echo $crud_list->title ?>" /> </td>
        </tr>
        <tr>
            <td><label for="author">Autor</label></td>
            <td><input class="bg-dark text-white" type="text" name="author" value="<?php echo $crud_list->author ?>" /> </td>
        </tr>
        <tr>
            <td><label for="author">ID Gatunku</label></td>
            <td><input class="bg-dark text-white" list="ids" type="text" name="author" value="<?php echo $crud_list->genre_id ?>" />
                <datalist id="ids">
                    <?php $i = 1; ?>
                    <?php foreach ($genre_list as $el) { ?>
                    <option value="<?php echo $i; $i += 1 ?>">
                        <?php } ?>
                </datalist>
            </td>
        </tr>
        <tr>
            <td><label for="pages">Liczba stron</label></td>
            <td><input class="bg-dark text-white" type="text" name="pages" value="<?php echo $crud_list->pages ?>" /> </td>
        </tr>
        <tr>
            <td><label for="year">Rok wydania</label></td>
            <td><input class="bg-dark text-white" type="text" name="year" value="<?php echo $crud_list->year ?>" /> </td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" class="btn btn-success" name="submit" value="Modyfikuj" /></td>
        </tr>
        </tbody>
    </table>


</form>


