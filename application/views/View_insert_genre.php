<h2 class="text-center"> Tworzenie nowego rekordu w tabeli bookGenre</h2>
<?php echo validation_errors(); ?>
<?php echo form_open('books/add_genre') ?>
<div class="row">
    <table class="table table-dark table-bordered table-striped table-hover">
        <thead>
        <tr>
            <th>Pole</th>
            <th>Zawartość</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><label for="genre_name">Nazwa gatunku</label></td>
            <td><input class="bg-dark text-white" type="text" name="genre_name"/> </td>
        </tr>
        </tbody>
        <tfoot>
            <th colspan="2"><input type="submit" class="btn btn-success" name="submit" value="Dodaj" /></th>
        </tfoot>
    </table>
</div>

</form>
