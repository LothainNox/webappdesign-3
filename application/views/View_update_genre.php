<h2 class="text-center"> Edycja gatunku o ID = <?php echo $crud_list->genre_index ?> </h2>
<?php echo validation_errors(); ?>
<?php echo form_open('books/update_genre') ?>

<table class="table table-dark table-bordered table-striped table-hover">
    <thead>
    <tr>
        <th>Pole</th>
        <th>Zawartość</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><label for="genre_index">indeks</label></td>
        <td><input class="bg-dark text-white" type="text" name="genre_index" readonly value="<?php echo $crud_list->genre_index ?>" /> </td>
    </tr>
    <tr>
        <td><label for="genre_name">Nazwa</label></td>
        <td><input class="bg-dark text-white" type="text" name="genre_name" value="<?php echo $crud_list->genre_name ?>" /> </td>
    </tr>
    <tr>
        <td colspan="2"><input type="submit" class="btn btn-success" name="submit" value="Modyfikuj" /></td>
    </tr>
    </tbody>
</table>


</form>


