<h2 class="text-center"> Tworzenie nowego rekordu w tabeli crudBiblioteka</h2>
<?php echo validation_errors(); ?>
<?php echo form_open('books/add') ?>
<div class="row">
    <br>
    <div class="col-md-8"></div>
    <div class="col-md-4"><h2>Dostępne gatunki</h2></div>
    <br>
</div>
<div class="row">
    <div class="col-md-8">
        <table class="table table-dark table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th>Pole</th>
                <th>Zawartość</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><label for="index">indeks</label></td>
                <td><input class="bg-dark text-white" type="text" name="index"/> </td>
            </tr>
            <tr>
                <td><label for="title">Tytuł</label></td>
                <td><input class="bg-dark text-white" type="text" name="title"/> </td>
            </tr>
            <tr>
                <td><label for="author">Autor</label></td>
                <td><input class="bg-dark text-white" type="text" name="author"/> </td>
            </tr>
            <tr>
                <td><label for="genre_id">ID gatunku</label></td>
                <td><input class="bg-dark text-white" list="ids" type="text" name="genre_id"/>
                    <datalist id="ids">
                        <?php $i = 1; ?>
                        <?php foreach ($genre_list as $el) { ?>
                            <option value="<?php echo $el['genre_index']; ?>">
                        <?php } ?>
                    </datalist>
                </td>
            </tr>
            <tr>
                <td><label for="pages">Liczba stron</label></td>
                <td><input class="bg-dark text-white" type="text" name="pages"/> </td>
            </tr>
            <tr>
                <td><label for="year">Rok wydania</label></td>
                <td><input class="bg-dark text-white" type="text" name="year"/> </td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" class="btn btn-success" name="submit" value="Dodaj" /></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-4">
        <table class="table table-dark table-bordered table-striped">
            <thead>
                <tr>
                    <th>Id</th>
                    <th></th>
                    <th>Nazwa</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; ?>
                <?php foreach ($genre_list as $el) { ?>
                    <tr>
                        <td> <?php echo $el['genre_index'] ?> </td>
                        <td>-></td>
                        <td> <?php echo $el['genre_name'] ?> </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

</form>
