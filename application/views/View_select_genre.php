<h2 class="text-center">Lista wszystkich gatunków książek</h2>
<table id="main_tab" class="table table-dark table-bordered table-striped table-hover">
    <thead>
    <tr>
        <th>Indeks</th>
        <th>Nazwa</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($crud_list as $el) { ?>
        <tr>
            <td> <?php echo $el['genre_index'] ?> </td>
            <td> <?php echo $el['genre_name'] ?> </td>
            <td><a href="update_genre_form/<?php echo $el['genre_index'] ?>" class="btn btn-warning">Edytuj</a></td>
            <td><a href="delete_genre/<?php echo $el['genre_index'] ?>" class="btn btn-danger">Kasuj</a></td>
        </tr>
    <?php } ?>
    <tfoot>
    <tr>
        <th colspan="100%"><a href="add_genre" class="btn btn-success"> Dodaj </a></th>
    </tr>
    </tfoot>
    </tbody>
</table>
